import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent implements OnInit {
  @Input('fileName') fileName: string = '';
  @Input('name') name: string = '';
  constructor() {}

  ngOnInit(): void {}

  avatarStyles() {
    let styles: any = {};

    if (this.fileName)
      styles.backgroundImage =
        'url(' + environment.BACKEND_BASE_URL + '/file/' + this.fileName + ')';

    return styles;
  }
}
