export interface userInterface {
  id: string;
  email: string;
  name: string;
  createdAt: string;
  updatedAt: string;
}
