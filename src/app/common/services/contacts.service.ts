import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, share, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class ContactsService {
  BASE_URL = `${environment.BACKEND_BASE_URL}/contacts`;
  constructor(private http: HttpClient, private authService: AuthService) {}

  get(options: { page: number }): Observable<{ total: number; data: any[] }> {
    return new Observable((observer) => {
      this.authService.getSession(true).subscribe((session) => {
        this.http
          .get<{ total: number; data: any[] }>(
            this.BASE_URL + `?page=${options.page}`,
            {
              headers: {
                authorization: `Bearer ${session.accessToken}`,
              },
            }
          )
          .subscribe((response: { total: number; data: any[] }) => {
            observer.next(response);
          });
      });
    });
  }
  search(text: string): Observable<any[]> {
    return new Observable((observer) => {
      this.authService.getSession(true).subscribe((session) => {
        this.http
          .get<any[]>(this.BASE_URL + '/' + text, {
            headers: {
              authorization: `Bearer ${session.accessToken}`,
            },
          })
          .subscribe((response: any[]) => {
            observer.next(response);
          });
      });
    });
  }

  getContact(id: number): Observable<any> {
    return new Observable((observer) => {
      this.authService.getSession(true).subscribe((session) => {
        this.http
          .get<{ total: number; data: any[] }>(
            this.BASE_URL + '/single/' + id,
            {
              headers: {
                authorization: `Bearer ${session.accessToken}`,
              },
            }
          )
          .subscribe((response: { total: number; data: any[] }) => {
            observer.next(response);
          });
      });
    });
  }

  delete(id: number): Observable<any> {
    return new Observable((observer) => {
      this.authService.getSession(true).subscribe((session) => {
        this.http
          .delete<any>(this.BASE_URL + '/' + id, {
            headers: {
              authorization: `Bearer ${session.accessToken}`,
            },
          })
          .subscribe((response: any) => {
            observer.next(response);
          });
      });
    });
  }

  create(data: {
    firstName: string;
    lastName: string;
    company?: string;
    pic?: string;
    phone: string;
  }) {
    return new Observable((observer) => {
      this.authService.getSession(true).subscribe((session) => {
        let ob = this.http.post(
          this.BASE_URL,
          { contact: data },
          {
            headers: {
              authorization: `Bearer ${session.accessToken}`,
            },
          }
        );

        ob.subscribe((response) => observer.next(response));
      });
    });
  }
  update(data: {
    firstName: string;
    lastName: string;
    company?: string;
    pic?: string;
    phone: string;
    id: number;
  }) {
    return new Observable((observer) => {
      this.authService.getSession(true).subscribe((session) => {
        let ob = this.http.put(
          this.BASE_URL + '/' + data.id,
          { contact: data },
          {
            headers: {
              authorization: `Bearer ${session.accessToken}`,
            },
          }
        );

        ob.subscribe((response) => observer.next(response));
      });
    });
  }
}
