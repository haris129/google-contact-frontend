import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, observable, Observable, share, Observer } from 'rxjs';
import { environment } from 'src/environments/environment';
import { userInterface } from '../interfaces/userInterface';

export interface userSession {
  user: userInterface;
  accessToken: string;
  refreshToken: string;
}
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  session: userSession | undefined;
  constructor(private http: HttpClient, private router: Router) {}

  getSession(refreshIfExpired = false): Observable<userSession> {
    return new Observable((observer: Observer<userSession>) => {
      let session: userSession = undefined as any;
      if (localStorage.getItem('session')) {
        session = JSON.parse(localStorage.getItem('session') as any);
      }
      if (refreshIfExpired && this.tokenExpired(session.accessToken)) {
        // then recreate token
        this.requestNewToken(session.refreshToken).subscribe(
          (response: { accessToken: string }) => {
            session.accessToken = response.accessToken;
            localStorage.setItem('session', JSON.stringify(session));

            observer.next(session);
          }
        );
      } else {
        observer.next(session);
      }
    });
  }

  requestNewToken(refreshToken: string) {
    return this.http.get<{ accessToken: string }>(
      `${environment.BACKEND_BASE_URL}/token`,
      {
        headers: {
          authorization: `Bearer ${refreshToken}`,
        },
      }
    );
  }

  tokenExpired(token: string) {
    const expiry = JSON.parse(atob(token.split('.')[1])).exp;
    return Math.floor(new Date().getTime() / 1000) >= expiry;
  }

  login(values: { email: string; password: string }): Observable<userSession> {
    let object = this.http
      .post<userSession>('http://localhost:3100/login', {
        email: values.email,
        password: values.password,
      })
      .pipe(share());
    object.subscribe((Response) => {
      console.log(Response.accessToken);
      if (Response.accessToken) {
        console.log(Response);
        localStorage.setItem('session', JSON.stringify(Response));
        this.router.navigateByUrl('/');
      }
    });
    return object;
  }

  logout() {
    return new Observable((observer: Observer<any>) => {
      this.getSession(true).subscribe((session) => {
        if (!session) {
          alert('Session expired.');
          observer.error('Session Expired');
        }
        let ob = this.http
          .get(`${environment.BACKEND_BASE_URL}/logout`, {
            headers: {
              authorization: 'Bearer ' + session.accessToken,
            },
          })
          .pipe(
            map((response: any) => {
              if (!response) return 'Done';
              return response;
            })
          );

        ob.subscribe((response) => {
          localStorage.removeItem('session');
          this.router.navigateByUrl('/login');
          observer.next(response);
        });
      });
    });
  }
}
