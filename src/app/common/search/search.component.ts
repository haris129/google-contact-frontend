import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatMenuTrigger } from '@angular/material/menu';
import { Router } from '@angular/router';
import { ContactsService } from '../services/contacts.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  text = '';
  searchResult: any[] = [];
  @ViewChild(MatMenuTrigger) matMenuTrigger: MatMenuTrigger | undefined;
  constructor(
    private contactsService: ContactsService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  search() {
    alert(this.text.length);

    if (this.text.length > 0) {
      this.contactsService.search(this.text).subscribe((response) => {
        this.searchResult = response;
      });
    }
  }

  contactSelected(contact: any) {
    this.router.navigateByUrl('/person/' + contact.id);
  }
}
