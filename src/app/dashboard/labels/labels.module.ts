import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LabelsComponent } from './labels.component';

@NgModule({
  declarations: [LabelsComponent],
  exports: [LabelsComponent],
  imports: [CommonModule],
})
export class LabelsModule {}
