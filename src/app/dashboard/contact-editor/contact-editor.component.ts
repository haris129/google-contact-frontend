import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactsService } from 'src/app/common/services/contacts.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-contact-editor',
  templateUrl: './contact-editor.component.html',
  styleUrls: ['./contact-editor.component.scss'],
})
export class ContactEditorComponent implements OnInit {
  contactToEdit: any;
  form: FormGroup = this.fb.group({
    id: '',
    pic: 'https://ssl.gstatic.com/s2/oz/images/sge/grey_silhouette.png',
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    company: [''],
    email: ['', Validators.email],
    phone: ['', Validators.required],
  });
  constructor(
    private fb: FormBuilder,
    private contactsService: ContactsService,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    public location: Location
  ) {
    this.route.paramMap.subscribe((params) => {
      let contactId = params.get('contactId');
      if (contactId) this.loadContact(Number(contactId));
    });
  }

  loadContact(contactId: number) {
    this.contactsService.getContact(contactId).subscribe((response) => {
      let data: any = {
        id: response.id,
        firstName: response.firstName,
        lastName: response.lastName,
        company: response.company,
        email: response.email,
        phone: response.phone,
      };
      if (response.pic) {
        data.pic = environment.BACKEND_BASE_URL + '/file/' + response.pic;
      } else {
        data.pic =
          'https://ssl.gstatic.com/s2/oz/images/sge/grey_silhouette.png';
      }

      this.form.patchValue(data);

      this.contactToEdit = this.form.value;
      this.form.updateValueAndValidity();
    });
  }

  onSubmit() {
    if (this.contactToEdit) {
      console.log(this.form.value.pic);
      this.contactsService
        .update(this.form.value)
        .subscribe((response: any) => {
          let statusMessage;

          if (response.error)
            statusMessage =
              'Error: ' +
              response.error.charAt(0).toUpperCase() +
              response.error.slice(1);
          else {
            statusMessage = 'Contact is updated successfully.';
            this.router.navigateByUrl('/');
          }

          this.snackBar.open(statusMessage, 'Ok', { duration: 1500 });
        });

      return;
    }

    this.contactsService.create(this.form.value).subscribe((response: any) => {
      let statusMessage;

      if (response.error)
        statusMessage =
          'Error: ' +
          response.error.charAt(0).toUpperCase() +
          response.error.slice(1);
      else {
        statusMessage = 'Contact is created successfully.';
        this.router.navigateByUrl('/');
      }

      this.snackBar.open(statusMessage, 'Ok', { duration: 1500 });
    });
  }

  handleFileSelect(event: any) {
    const file = event.target.files[0];
    const reader: any = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.form.patchValue({ pic: reader.result });
      this.form.updateValueAndValidity();
    };
  }

  ngOnInit(): void {}

  areContactsSame(contact1: any, contact2: any) {
    return (
      contact1.id === contact2.id &&
      contact1.pic === contact2.pic &&
      contact1.firstName === contact2.firstName &&
      contact1.lastName === contact2.lastName &&
      contact1.company === contact2.company &&
      contact1.email === contact2.email &&
      contact1.phone === contact2.phone
    );
  }
}
