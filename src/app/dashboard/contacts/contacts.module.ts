import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ContactsComponent } from './contacts.component';
import { AvatarModule } from 'src/app/common/avatar/avatar.module';

@NgModule({
  declarations: [ContactsComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatCheckboxModule,
    FlexModule,
    MatMenuModule,
    MatIconModule,
    FormsModule,
    MatProgressSpinnerModule,
    AvatarModule,
  ],
  exports: [ContactsComponent],
})
export class ContactsModule {}
