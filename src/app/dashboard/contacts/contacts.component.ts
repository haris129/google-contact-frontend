import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ContactsService } from 'src/app/common/services/contacts.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
})
export class ContactsComponent implements OnInit {
  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'company',
    'email',
    'phone',
    'actions',
  ];
  dataSource: any[] = [];
  totalContacts: number = 0;
  currentPage = 1;
  totalRecordsPerPage = 2;
  environment = environment;
  loading = false;
  constructor(
    private contactsService: ContactsService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadContacts();
  }

  totalPages() {
    return Math.ceil(this.totalContacts * (1 / this.totalRecordsPerPage));
  }

  delete(contact: any) {
    this.contactsService.delete(contact.id).subscribe(() => {
      this.dataSource = this.dataSource.filter((c) => c.id != contact.id);
      this.snackBar.open('Contact is deleted successfully', 'Okay', {
        duration: 1400,
      });
    });
  }
  edit(contact: any) {
    this.router.navigateByUrl('/person/' + contact.id);
  }

  loadContacts() {
    this.loading = true;
    this.contactsService
      .get({ page: this.currentPage })
      .subscribe((response) => {
        this.loading = false;
        this.dataSource = response.data;
        this.totalContacts = response.total;
      });
  }

  getSelectedContacts() {
    return this.dataSource.filter((contact) => contact.selected);
  }

  selectAll() {
    this.dataSource = this.dataSource.map((contact) => {
      contact.selected = true;
      return contact;
    });
  }

  unselectAll() {
    this.dataSource = this.dataSource.map((contact) => {
      contact.selected = false;
      return contact;
    });
  }

  deleteSelectedContacts() {
    this.getSelectedContacts().forEach((contact) => {
      this.delete(contact);
    });
  }

  loadMore() {
    ++this.currentPage;
    this.loadContacts();
  }
}
