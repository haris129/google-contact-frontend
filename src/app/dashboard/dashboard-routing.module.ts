import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactEditorComponent } from './contact-editor/contact-editor.component';
import { ContactEditorModule } from './contact-editor/contact-editor.module';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactsModule } from './contacts/contacts.module';
import { DashboardComponent } from './dashboard.component';
import { LabelsComponent } from './labels/labels.component';
import { LabelsModule } from './labels/labels.module';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', component: ContactsComponent },
      { path: 'new', component: ContactEditorComponent },
      { path: 'person/:contactId', component: ContactEditorComponent },
      { path: 'labels', component: LabelsComponent },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ContactsModule,
    ContactEditorModule,
    LabelsModule,
  ],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
