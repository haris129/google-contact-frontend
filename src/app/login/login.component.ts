import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { catchError, throwError } from 'rxjs';
import { AuthService } from '../common/services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    public authService: AuthService,
    private snackbar: MatSnackBar
  ) {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {}

  onFormSubmit(event: Event) {
    event.preventDefault();
    event.stopImmediatePropagation();
    if (this.form.valid) {
      this.authService
        .login({
          email: this.form.value.email,
          password: this.form.value.password,
        })
        .pipe(
          catchError((error: HttpErrorResponse) => {
            return throwError(
              () =>
                new Error(
                  error.status === 401 ? 'invalid credentials' : error.message
                )
            );
          })
        )
        .subscribe({
          next: (Response) => {},
          error: (e) => {
            this.snackbar.open(e.message, 'error', { duration: 2000 });
          },
        });
    }
  }
}
