import { Component } from '@angular/core';
import { AuthService, userSession } from './common/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'google-contacts-frontend';
  constructor(public authService: AuthService) {}
  logout() {
    this.authService.logout()?.subscribe({
      next: () => {},
      error: (error) => {
        alert(error.message);
      },
    });
  }
}
